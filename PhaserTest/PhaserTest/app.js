var Game = (function () {
    function Game() {
        this.game = new Phaser.Game(800, 600, Phaser.AUTO, 'content', { preload: this.preload, create: this.create, update: this.update });
    }
    Game.prototype.preload = function () {
        this.game.load.image('sky', 'assets/sky.png');
        this.game.load.image('player', 'assets/player.png');
        this.game.load.image('ground', 'assets/ground.png');
        this.game.load.image('prop', 'assets/prop.png');
        this.random = new Phaser.RandomDataGenerator();
        this.obstacleList = new Array();
    };
    Game.prototype.create = function () {
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.sky = this.game.add.sprite(0, 0, 'sky');
        this.ground = this.game.add.sprite(0, this.game.world.height - 100, 'ground');
        this.game.physics.arcade.enable(this.ground);
        this.ground.body.immovable = true;
        this.player = this.game.add.sprite(25, this.game.world.height - 300, 'player');
        this.player.scale.x = 0.5;
        this.player.scale.y = 0.5;
        this.game.physics.arcade.enable(this.player);
        this.player.body.bounce.y = 0.2;
        this.player.body.gravity.y = 1250;
        this.player.body.collideWorldBounds = true;
        this.playerOnGround = true;
        this.spawnerCooldown = 6;
        this.spawnCd = this.spawnerCooldown;
        this.originalSpawnerCooldown = this.spawnerCooldown;
        this.spawnerCooldownDecay = 0.1;
        this.spawnerCooldownVariation = 0.5;
        this.spawnerMinCooldown = 1;
        this.enemySpeed = 250;
        this.enemySpeedIncrement = 10;
        this.enemyMaxSpeed = 750;
        this.gameIsRunning = true;
        this.survivalTime = 0;
        this.survivalText = this.game.add.text(0, this.game.world.height - 50, '');
    };
    Game.prototype.update = function () {
        var _this = this;
        // COLLISIONS
        var hitGround = this.game.physics.arcade.collide(this.player, this.ground);
        var hitObstacle;
        for (var i = 0; i < this.obstacleList.length; i++) {
            if (this.obstacleList[i] != null)
                hitObstacle = this.game.physics.arcade.collide(this.player, this.obstacleList[i]);
            if (hitObstacle) {
                this.obstacleList[i].kill();
                this.obstacleList[i] = null;
                this.player.kill();
                this.gameIsRunning = false;
                this.resetText = this.game.add.text(0, 50, 'You broke it! Press \'R\' to restart.');
            }
        }
        var obstacleHitGround;
        this.obstacleList.forEach(function (item, index) {
            if (item != null)
                obstacleHitGround = _this.game.physics.arcade.collide(item, _this.ground);
        });
        // COLLISIONS
        // RESET GAME
        if (!this.gameIsRunning && this.game.input.keyboard.isDown(Phaser.KeyCode.R)) {
            for (var i = 0; i < this.obstacleList.length; i++) {
                if (this.obstacleList[i] != null) {
                    this.obstacleList[i].kill();
                    this.obstacleList[i] = null;
                }
            }
            this.player = this.game.add.sprite(25, this.game.world.height - 300, 'player');
            this.player.scale.x = 0.5;
            this.player.scale.y = 0.5;
            this.game.physics.arcade.enable(this.player);
            this.player.body.bounce.y = 0.2;
            this.player.body.gravity.y = 1250;
            this.player.body.collideWorldBounds = true;
            this.playerOnGround = true;
            this.spawnerCooldown = this.originalSpawnerCooldown;
            this.spawnCd = this.spawnerCooldown;
            this.enemySpeed = 250;
            this.gameIsRunning = true;
            this.resetText.kill();
            this.resetText = null;
            this.survivalTime = 0;
        }
        // RESET GAME
        // MAIN LOOP
        if (this.gameIsRunning) {
            // DELTA TIME
            this.deltaTime = this.time.elapsed / 1000;
            this.survivalTime += this.deltaTime;
            this.survivalText.text = 'Survival time: ' + Math.round(this.survivalTime).toString();
            // DELTA TIME
            // OBSTACLE MOVEMENT
            if (this.enemySpeed < this.enemyMaxSpeed)
                this.enemySpeed += this.enemySpeedIncrement * this.deltaTime;
            // OBSTACLE MOVEMENT
            // INPUT
            if (this.game.input.keyboard.isDown(Phaser.KeyCode.UP) && hitGround) {
                this.player.body.velocity.y = -600;
            }
            // INPUT
            // CHECK OUT OF SCREEN
            this.obstacleList.forEach(function (item, index) {
                if (item != null) {
                    if (item.position.x < -136) {
                        item.kill();
                        item = null;
                    }
                }
            });
            // CHECK OUT OF SCREEN
            // CHECK COOLDOWNS AND SPAWN
            if (this.spawnCd > 0)
                this.spawnCd -= this.deltaTime;
            else {
                var obstacle = this.game.add.sprite(this.game.world.width, this.game.world.height - 200, 'prop');
                obstacle.scale.x = 0.5;
                obstacle.scale.y = 0.5;
                this.game.physics.arcade.enable(obstacle);
                obstacle.body.bounce.y = 0;
                obstacle.body.drag.x = 0;
                obstacle.body.drag.y = 0;
                obstacle.body.velocity.x = -this.enemySpeed;
                obstacle.body.gravity.y = 1250;
                obstacle.body.collideWorldBounds = false;
                this.obstacleList.push(obstacle);
                this.spawnCd = this.spawnerCooldown + this.random.between(-this.spawnerCooldownVariation, this.spawnerCooldownVariation);
            }
            if (this.spawnerCooldown > this.spawnerMinCooldown)
                this.spawnerCooldown -= this.spawnerCooldownDecay * this.deltaTime;
        }
        // MAIN LOOP
    };
    return Game;
}());
window.onload = function () {
    var g = new Game();
};
//# sourceMappingURL=app.js.map