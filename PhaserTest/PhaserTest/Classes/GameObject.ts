﻿class GameObject extends Object {

    g: Phaser.Game;
    name: string;
    posx: number;
    posy: number;
    rotation: number;
    myList: GameObject[];
    timeAlive: number;
    tag: string;
    sprite: Phaser.Sprite;

    constructor(list: GameObject[], game: Phaser.Game) {
        super();
        this.g = game;
        this.myList = list;
        list.push(this);
    }

    update(dt: number) {
        this.timeAlive += dt;
    }

    translate(x: number, y: number) {
        this.posx += x;
        this.posy += y;

        if (this.sprite != null) {
            this.sprite.position.x += x;
            this.sprite.position.y += y;
        }
    }

    setPosition(x: number, y: number) {
        this.posx = x;
        this.posy = y;

        if (this.sprite != null) {
            this.sprite.position.x = x;
            this.sprite.position.y = y;
        }
    }

    rotate(angle: number) {
        this.rotation += angle;
        if (this.sprite != null) {
            this.sprite.rotation += angle;
        }
    }

    setRotation(angle: number) {
        this.rotation = angle;
        if (this.sprite != null) {
            this.sprite.rotation = angle;
        }
    }

    setTag(t: string) {
        this.tag = t;
    }

    loadSprite(filename: string) {
        this.g.load.image(this.name + ' sprite', filename);
        this.sprite = this.g.add.sprite(this.posx, this.posy, this.name + ' sprite');
    }

    destroy() {
		this.myList.forEach((item, index) => {
            if (item == this)
                this.myList[index] = null;
        })
        this.sprite.destroy();
        this.sprite = null;
    }
}