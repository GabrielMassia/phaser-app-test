var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var GameObject = (function (_super) {
    __extends(GameObject, _super);
    function GameObject(list, game) {
        _super.call(this);
        this.g = game;
        this.myList = list;
        list.push(this);
    }
    GameObject.prototype.update = function (dt) {
        this.timeAlive += dt;
    };
    GameObject.prototype.translate = function (x, y) {
        this.posx += x;
        this.posy += y;
        if (this.sprite != null) {
            this.sprite.position.x += x;
            this.sprite.position.y += y;
        }
    };
    GameObject.prototype.setPosition = function (x, y) {
        this.posx = x;
        this.posy = y;
        if (this.sprite != null) {
            this.sprite.position.x = x;
            this.sprite.position.y = y;
        }
    };
    GameObject.prototype.rotate = function (angle) {
        this.rotation += angle;
        if (this.sprite != null) {
            this.sprite.rotation += angle;
        }
    };
    GameObject.prototype.setRotation = function (angle) {
        this.rotation = angle;
        if (this.sprite != null) {
            this.sprite.rotation = angle;
        }
    };
    GameObject.prototype.setTag = function (t) {
        this.tag = t;
    };
    GameObject.prototype.loadSprite = function (filename) {
        this.g.load.image(this.name + ' sprite', filename);
        this.sprite = this.g.add.sprite(this.posx, this.posy, this.name + ' sprite');
    };
    GameObject.prototype.destroy = function () {
        var _this = this;
        this.myList.forEach(function (item, index) {
            if (item == _this)
                _this.myList[index] = null;
        });
        this.sprite.destroy();
        this.sprite = null;
    };
    return GameObject;
}(Object));
//# sourceMappingURL=GameObject.js.map